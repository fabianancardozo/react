import { useState } from 'react';
import './App.css';
import TaskForm from './components/TaskForm';
import TaskList from './components/TaskList';
import { Task } from './interfaces/Task';
import logo from './logo.png'

function App(){

const [tasks, setTasks] = useState <Task[]>([
  {
    id: 1,
    title: 'Task Title',
    description: 'lorem',
    completed: false,
  }
])


const getCurrentTimesTamp = (): number => new Date().getTime();


const addANewTask = (task: Task) => 
  setTasks ([...tasks, {...task, id: getCurrentTimesTamp(), completed: false}]);


const deleteATask = (id: number) => setTasks (tasks.filter(task => task.id !== id));


//En return recorro el array tasks, con un map, dentro de este creo un 'task' y le digo que por cada task que este recoriendo que por ahora pinte un div
  return (
    <div className="bg-dark text-white" style={{height:'100vh'}}>
      
      <nav className='navbar navbar-dark bg-primary'>
        <div className='container'>
          <a href='/' className='navbar-brand'><h1>Benvenuto</h1></a>
          <img src={logo} alt="react logo" style={{width:'4rem'}} />
        </div>
      </nav>
    
      <main className='container p-4'>
       <div className="row">
        <div className='col-md-4'>
            <TaskForm addANewTask={addANewTask}/>
          </div>

          <div className='col-md-8'>
            <div className='row'>
              <TaskList tasks={tasks} deleteATask={deleteATask}/>
            </div>  
        </div>
       </div>
          
      </main>
      

    </div>
  );
}

export default App;
